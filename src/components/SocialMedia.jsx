import React from "react";
import {
	BsTwitter,
	BsInstagram,
	BsLinkedin,
	BsNewspaper,
} from "react-icons/bs";
import { FaFacebookF, FaGithub, FaGitlab } from "react-icons/fa";

const SocialMedia = () => (
	<div className="app__social">
		<div>
			<a href="https://www.linkedin.com/in/aldizulfikar/" target="_blank">
				<BsLinkedin />
			</a>
		</div>
		<div>
			<a href="https://github.com/AldiZulfikar" target="_blank">
				<FaGithub />
			</a>
		</div>
		<div>
			<a href="https://gitlab.com/AldiZulfikar" target="_blank">
				<FaGitlab />
			</a>
		</div>
	</div>
);

export default SocialMedia;
