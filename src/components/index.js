import NavigationDots from './NavigationDots';
import SocialMedia from './SocialMedia';
import Navbar from './Navbar/navbar';

export {
  NavigationDots,
  SocialMedia,
  Navbar,
};